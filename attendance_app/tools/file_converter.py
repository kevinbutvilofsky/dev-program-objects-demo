import sys
import os
# DO not hardcode '/Users/mariamarquina/Documents/dev_program/dev_program/attendance_app' 
# problem is to calculate '/Users/mariamarquina/Documents/dev_program/dev_program/attendance_app'
file = (__file__)
dirname = (os.path.dirname(__file__))
# take off "/tools" from '/Users/mariamarquina/Documents/dev_program/dev_program/attendance_app/tools'
dirname = dirname[::-1].split('/', 1)[-1][::-1]
sys.path.append(dirname)

# TODO do not hardcode find this '/Users/mariamarquina/Documents/dev_program/dev_program/attendance_app/src'
sys.path.append('/Users/mariamarquina/Documents/dev_program/dev_program/attendance_app/src')


from src.logger_wrapper import setup_logger
from src.teams_files_reader import load_data

logger =  setup_logger(__name__)

def main():
    logger.info("starting main")
    # TODO: do my stuff
    load_data()
    logger.info("finishing main")




if __name__ == '__main__':
    main()































# import sys
# import os
# # read current file's directory; then make sure path is one level up
# print(os.path.dirname(__file__))
# print()
# path = os.path.dirname(__file__)[::-1].split('/', 1)[-1][::-1]

# # now put `path` in list sys.path
# sys.path.extend([path, f'{path}/src'])
# print(sys.path)

# import function setup_logger from module `logger_wrapper`.
# `logger_wrapper` is located in src folder


# from src.logger_wrapper import setup_logger
# from src.teams_files_reader import load_data

# logger = setup_logger(__name__)

# def main():
#     """orchestrates tools functionality"""
#     logger.info("starting main")
#     load_data()
#     logger.info("finishing main")

# if __name__ == '__main__':
#     main()